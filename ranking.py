from PPlay.window import *
from PPlay.gameimage import *
from PPlay.sprite import *
from PPlay.mouse import *


def ranking():

    window = Window(1920, 1080)
    window.set_title("Space Invaders")

    keyboard = window.get_keyboard()

    mouse = Mouse()

    click = 0
    click_delay = 1.0

    arquivo = open("ranking.txt", "r")
    linha = arquivo.readline()
    n_linhas = 0

    while(True):
        if keyboard.key_pressed("esc"):
            break

        if mouse.is_button_pressed(1) and click >= click_delay:

            click = 0

        window.draw_text(
            "RANKING", window.width / 2 - 70, 150,  30, (255, 255, 255))
        while linha:
            valores = linha.split('-')
            window.draw_text(
                "Nome: " + valores[0], window.width / 4, 270 + 50 * n_linhas,  30, (255, 255, 255))
            window.draw_text(
                "Score: " + valores[1].removesuffix('\n'), window.width - window.width / 4 - 150, 270 + 50 * n_linhas,  30, (255, 255, 255))
            linha = arquivo.readline()
            n_linhas += 1

        click += window.delta_time()
        window.update()
    arquivo.close()
