from PPlay.window import *
from PPlay.gameimage import *
from PPlay.sprite import *
from PPlay.mouse import *
from PPlay.keyboard import *

from game import jogar
from ranking import ranking

from difficulty import change_difficulty
# Gerando a janela do jogo

window = Window(1920, 1080)
window.set_title("Space Invaders")

mouse = Mouse()

background = GameImage('./assets/images/background.jpg')
start_btn = Sprite('./assets/images/start-btn.png')
difficulty_btn = Sprite('./assets/images/difficulty-btn.png')
ranking_btn = Sprite('./assets/images/ranking-btn.png')
exit_btn = Sprite('./assets/images/exit-btn.png')

start_btn.x = background.width / 2 - start_btn.width / 2
start_btn.y = window.height / 2 - window.height / 8

difficulty_btn.x = background.width / 2 - difficulty_btn.width / 2
difficulty_btn.y = start_btn.y + start_btn.height + 20

ranking_btn.x = background.width / 2 - ranking_btn.width / 2
ranking_btn.y = difficulty_btn.y + difficulty_btn.height + 20

exit_btn.x = background.width / 2 - exit_btn.width / 2
exit_btn.y = ranking_btn.y + ranking_btn.height + 20

playing = False

click = 0
click_delay = 1.0
difficulty = 1


# Game Loop
while(True):

    if playing:
        playing = jogar(difficulty)

    if mouse.is_button_pressed(1) and click >= click_delay:
        if mouse.is_over_area([start_btn.x, start_btn.y], [start_btn.x + start_btn.width, start_btn.y + start_btn.height]):
            playing = True

        if mouse.is_over_area([difficulty_btn.x, difficulty_btn.y], [difficulty_btn.x + difficulty_btn.width, difficulty_btn.y + difficulty_btn.height]):
            click = 0
            difficulty = change_difficulty()

        if mouse.is_over_area([ranking_btn.x, ranking_btn.y], [ranking_btn.x + ranking_btn.width, ranking_btn.y + ranking_btn.height]):
            click = 0
            ranking()

        if mouse.is_over_area([exit_btn.x, exit_btn.y], [exit_btn.x + exit_btn.width, exit_btn.y + exit_btn.height]):
            click = 0
            window.close()

    background.draw()
    start_btn.draw()
    difficulty_btn.draw()
    ranking_btn.draw()
    exit_btn.draw()

    click += window.delta_time()

    if not playing:
        window.update()
