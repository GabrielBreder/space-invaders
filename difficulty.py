from PPlay.window import *
from PPlay.gameimage import *
from PPlay.sprite import *
from PPlay.mouse import *


def change_difficulty():

    window = Window(1920, 1080)
    window.set_title("Space Invaders")

    keyboard = window.get_keyboard()

    easy_btn = Sprite('./assets/images/easy-btn.png')
    medium_btn = Sprite('./assets/images/medium-btn.png')
    hard_btn = Sprite('./assets/images/hard-btn.png')

    easy_btn.x = window.width / 2 - easy_btn.width / 2
    medium_btn.x = window.width / 2 - medium_btn.width / 2
    hard_btn.x = window.width / 2 - hard_btn.width / 2

    easy_btn.y = window.height / 3 - easy_btn.height / 2
    medium_btn.y += easy_btn.height + easy_btn.y + 50
    hard_btn.y += medium_btn.height + medium_btn.y + 50

    mouse = Mouse()

    click = 0
    click_delay = 1.0

    while(True):
        if keyboard.key_pressed("esc"):
            break

        if mouse.is_button_pressed(1) and click >= click_delay:
            click = 0
            if mouse.is_over_area([easy_btn.x, easy_btn.y], [easy_btn.x + easy_btn.width, easy_btn.y + easy_btn.height]):
                return 1

            if mouse.is_over_area([medium_btn.x, medium_btn.y], [medium_btn.x + medium_btn.width, medium_btn.y + medium_btn.height]):
                return 2

            if mouse.is_over_area([hard_btn.x, hard_btn.y], [hard_btn.x + hard_btn.width, hard_btn.y + hard_btn.height]):
                return 3

        click += window.delta_time()
        easy_btn.draw()
        medium_btn.draw()
        hard_btn.draw()
        window.update()
