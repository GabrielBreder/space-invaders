from calendar import month
from PPlay.window import *
from PPlay.gameimage import *
from PPlay.sprite import *
from PPlay.mouse import *
import random


def cria_monstros(matriz_monstros):
    enemy = Sprite("./assets/images/enemy_android.png")
    padding_x = 10
    for i in range(4):
        padding_y = 10
        for j in range(4):
            enemy = Sprite("./assets/images/enemy_android.png")

            enemy.x = i * enemy.width + padding_x
            enemy.y = j * enemy.height + padding_y

            matriz_monstros[i].append(enemy)
            padding_y += enemy.height / 4
        padding_x += enemy.width / 4


def bateu_parede(matriz_monstros):
    for i in range(len(matriz_monstros)):
        for j in range(len(matriz_monstros[i])):
            matriz_monstros[i][j].y += matriz_monstros[i][j].height / 2


def shot_monstro(matriz_monstros, random_num_linha, random_num_coluna):
    shot_monstro.x = matriz_monstros[random_num_linha][random_num_coluna].x + \
        matriz_monstros[random_num_linha][random_num_coluna].width / 2
    shot_monstro.y = matriz_monstros[random_num_linha][random_num_coluna].y + \
        matriz_monstros[random_num_linha][random_num_coluna].height


score = 0


def jogar(difficulty):
    window = Window(1920, 1080)
    window.set_title("Space Invaders")

    keyboard = window.get_keyboard()

    mov_monstros = difficulty

    vel_player = 700
    vel_shot = int(1000 / difficulty)
    vidas = 3

    background = GameImage('./assets/images/background.jpg')
    shot_monstro = Sprite("./assets/images/shot_enemy.png")

    player = Sprite("./assets/images/player.png")
    player.x = window.width / 2 - player.width / 2
    player.y = window.height - player.height * 1.5

    shots = []
    time_reload = difficulty / 2
    recarga_player = 0

    time_vidas = 1.0
    recarga_vidas = 0

    matriz_monstros = [[], [], [], []]

    cria_monstros(matriz_monstros)

    def chose_monster():
        linha = random.choice(matriz_monstros)
        monstro = random.choice(linha)
        return monstro

    monstro = chose_monster()

    def reset_shot_monster(monstro):
        shot_monstro.x = monstro.x + monstro.width / 2
        shot_monstro.y = monstro.y + monstro.height / 2

    def bateu():
        global score
        for i in range(len(matriz_monstros)):
            for j in range(len(matriz_monstros[i])):
                for k in range(len(shots)):
                    if matriz_monstros[i][j].collided_perfect(shots[k]):

                        matriz_monstros[i].remove(matriz_monstros[i][j])
                        shots.remove(shots[k])
                        score += 1
                        return

    reset_shot_monster(monstro)

    def write_score():
        arquivo = open("ranking.txt", "a")
        name = input("Digite seu nome: ")
        arquivo.write(name + '-' + str(score) + '\n')
        arquivo.close()

    while(True):
        if keyboard.key_pressed("esc"):
            return False

        if keyboard.key_pressed("a"):
            player.x -= vel_player * window.delta_time()

        if keyboard.key_pressed("d"):
            player.x += vel_player * window.delta_time()

        if player.x < 0:
            player.x = window.width
        elif player.x > window.width:
            player.x = 0

        if keyboard.key_pressed("space") and recarga_player >= time_reload:
            shot = Sprite("./assets/images/shot.png")
            shot.x = player.x + player.width / 2
            shot.y = player.y
            shots.append(shot)
            recarga_player = 0

        background.draw()
        for shot in shots:

            if shot.y < 0:
                shots.remove(shot)
                break
            shot.y -= vel_shot * window.delta_time()
            shot.draw()

        recarga_player += window.delta_time()

        for shot in shots:
            shot.draw()
            if shot.collided_perfect(shot_monstro):
                reset_shot_monster(monstro)
                shots.remove(shot)
                break

        bateu()

        for i in range(len(matriz_monstros)):
            for j in range(len(matriz_monstros[i])):
                matriz_monstros[i][j].draw()

        for i in range(len(matriz_monstros)):
            for j in range(len(matriz_monstros[i])):
                matriz_monstros[i][j].x += mov_monstros
                if matriz_monstros[i][j].x > window.width - matriz_monstros[i][j].width - 5 or matriz_monstros[i][j].x < 5:
                    mov_monstros *= -1
                    bateu_parede(matriz_monstros)

                if matriz_monstros[i][j].y >= player.y:
                    vidas = 0

        shot_monstro.y += difficulty * 100 * window.delta_time()

        if shot_monstro.y > window.height:
            try:
                monstro = chose_monster()
            except:
                pass
            reset_shot_monster(monstro)

        if shot_monstro.collided_perfect(player) and recarga_vidas >= time_vidas:
            vidas -= 1
            recarga_vidas = 0
            player.x = window.width / 2 - player.width / 2

            try:
                monstro = chose_monster()
            except:
                pass
            reset_shot_monster(monstro)

        recarga_vidas += window.delta_time()

        if matriz_monstros == [[], [], [], []]:
            cria_monstros(matriz_monstros)
            if mov_monstros < 0:
                mov_monstros *= -1
            mov_monstros += 1

        if vidas == 0:
            write_score()
            return False

        window.draw_text("Pontuação:  " + str(score), window.width -
                         230, 0,  30, (255, 255, 255))
        window.draw_text("Vidas: " + str(vidas), 0, 0,  30, (255, 255, 255))
        shot_monstro.draw()
        player.draw()
        window.update()
